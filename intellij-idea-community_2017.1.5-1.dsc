-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: intellij-idea-community
Binary: intellij-idea-community
Architecture: all
Version: 2017.1.5-1
Maintainer: Marcel Michael Kapfer <marcelmichaelkapfer@yahoo.co.nz>
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 7.0.50~)
Package-List:
 intellij-idea-community deb devel optional arch=all
Checksums-Sha1:
 2af60d7bb0d2765946394c4200dac6e38abe4b29 5641 intellij-idea-community_2017.1.5.orig.tar.gz
 24d124a4696028895881847c5296c613f0afd730 2648 intellij-idea-community_2017.1.5-1.debian.tar.xz
Checksums-Sha256:
 93646f0ed33fb1c62473e12c18f9fa7d90ade1ec3ec2c00b92eafd9fac728882 5641 intellij-idea-community_2017.1.5.orig.tar.gz
 3d41f4ba68c2b6d4f03a2cc32fb4de7219c17403a10d7a879bb3af50e22ab349 2648 intellij-idea-community_2017.1.5-1.debian.tar.xz
Files:
 38a2df9af92629b31beaa46222d3e00e 5641 intellij-idea-community_2017.1.5.orig.tar.gz
 2023ce5e128969924a92def2d8c54945 2648 intellij-idea-community_2017.1.5-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----

iQIcBAEBCAAGBQJZZlesAAoJECuF2UqwCcGYOY0QAIUWTI+qeFMA/3461PIBqmuJ
8Nz51hQ98a+9bFXH7gA2EcUnLqsQMA/sYZrCejGYp37iic8NHl1L/vfK6RI/S3A2
Paq+3niXujdm+FEEFO+J4Ol5Obf+MUlWd3krQC9TcnGPMOJjE1FBfe1jFbuIZp/g
jLkYzHisiEaJJ3iBrqemNQpamJmQkH1wLdhr2SYu/qMhjesc2NXfmAkTbFy/hZAB
LYaorvLiPlO+PvJyl70TrJp7qB2BKeSr1fDY2PNNZ33siIMpHPWmGvm27gnmplUS
+fXhi88CZCFwBJGrnXFLn9n7vWavpuCr0vfBSAxuRkV0qBV8vjMjrhVBHA/SfqMw
K8HsCYd22Hjvmo6IpxKOnl4eSCQjH2Ic0T1+u09LPfTSml7CnaHsufANlG75anlN
PRASMEgl4ozjs5HYnEP08gs9jwz6K2d/zyOA5uQY/KlrptWaht0iYOoL7DWAofe0
U2G0fWtCRu9O2xUVDYNAdArp5is88pBMAtmIcL/uOa6ZH9UOCsdLOuFGgyeEv2Kb
ZvusuXtdlJGKISDAF/l453rq3xI960HRfxLfb29bYv3KYzisbO1yzqBmh5YkfU1k
hbzTcNEZBrEg1ISe8CjxFvGJefGsNQqkD81DfIhGNT6kL/HEa1imHP5Ni07XcHbP
iYjQil7be/eZVWq1w0yv
=/XIK
-----END PGP SIGNATURE-----
